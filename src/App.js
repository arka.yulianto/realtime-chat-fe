import React, { useEffect, useState } from 'react'
import './app.css'
import socketIOClient from 'socket.io-client'

const socketurl = 'http://192.168.100.67:4001'
const socket = socketIOClient(socketurl);

function App() {
  
  const [chats, setChats] = useState([])
  const [chat, setChat] = useState({
    id: '',
    name: '',
    message: ''
  })
  const [typing, setTyping] = useState('')
  const [users, setUsers] = useState([])

  const submitChat = () => {
    
    const theChat = [
      ...chats,
      chat
    ]

    setChats(theChat)

    setChat({
      ...chat,
      message: ''
    })

    socket.emit('update chat', theChat)
    socket.emit('typing', '')
  }

  const handleChange = (name) => (e) => {

    if (e.target.value) {
      setTyping(`${chat.name}`)
    } else {
      setTyping('')
    }

    name === 'message' && socket.emit('typing', e.target.value ? chat.name : '')


    setChat({
      ...chat,
      [name]: e.target.value
    })

  }

  const handleSaveUser = () => {
    const id = Math.random() * 100
    
    setChat({
      ...chat,
      id
    })
    
    localStorage.setItem('name', chat.name)
    localStorage.setItem('id', id)
    
    const theUsers = [
      ...users,
      {
        name: chat.name,
        id
      }
    ]

    socket.emit('typing', '')
    socket.emit('new user', theUsers)

  }

  useEffect(() => {

    socket.on('update chat', (chats) => {
      setChats(chats)
    })

    socket.on('typing', typing => {
      setTyping(typing)
    })

    if (localStorage.getItem('id')) {

      setChat({
        ...chat,
        id: localStorage.getItem('id'),
        name: localStorage.getItem('name')
      })

    }

  }, [])

  return !localStorage.getItem('id') ? (
    <div id="frame">
      <div className="introduction">
        <input type="text" onChange={handleChange('name')} value={chat.name} className="flat-input" placeholder="Who's your name"/>
        <button className="submit" onClick={handleSaveUser}>Save</button>
      </div>
    </div>
  ) : (
    <div id="frame">
      <div id="sidepanel">
        <div id="profile">
          <div className="wrap">
            <img id="profile-img" src={`https://i.pravatar.cc/100?img=${chat.id}`} className="online" alt="" />
            <p>{chat.name}</p>
          </div>
        </div>
        <div id="contacts">
          <p style={{ marginLeft: 20, marginBottom: 20 }}>All Users</p>
          <ul>
            {
              users && users.map(user => {
                return (
                  <li className="contact" key={user.id}>
                    <div className="wrap">
                      <img src={`https://i.pravatar.cc/100?img=${user.id}`} alt="" />
                      <div className="meta">
                        <p style={{marginBottom: 20, paddingTop: 5}} className="name">{user.name}</p>
                      </div>
                    </div>
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
      <div className="content">
        <div className="contact-profile">
        <p className="chat-name">Chat Group Room 101<br />{ typing && <small>{typing} Typing...</small> }</p>
        </div>
        <div className="messages">
          <ul>
            {
              chats && chats.map((item, index) => {
                return (<li className={item.id === chat.id ? 'replies' : 'sent'} key={index}>
                  <img src={`https://i.pravatar.cc/100?img=${item.id}`} alt={item.name} />
                  <p>{item.message}</p>
                </li>)
              })
            }
          </ul>
        </div>
        <div className="message-input">
          <div className="wrap">
          <input onKeyDown={(e) => e.key === 'Enter' && submitChat()} type="text" placeholder="Write your message..." value={chat.message} onChange={handleChange('message')} />
          <button className="submit" onClick={submitChat}><i className="fa fa-paper-plane" aria-hidden="true"></i></button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
